var can1;
var context
var img=new Image();
img.src="./图像/star2.png";
var count=30;
var stars_array=[];
var reset_counter=0;

function Star(){
    this.x;
    this.y;
    this.star;
    this.size;
}

Star.prototype.init=function(){
    this.x=Math.floor(Math.random()*1800);
    this.y=Math.floor(Math.random()*800);
    this.star=Math.floor(Math.random()*6);
    
    this.size=Math.floor();
    this.xsp=Math.random()*0.5-0.25;
    this.ysp=Math.random()*0.5-0.25;

    var temp=Math.random();
    if(temp>0.5 && temp<0.95){temp-=0.4;}
    this.size=temp*5;
}

Star.prototype.draw=function(){
    
    if(this.x>1920 || this.y>1080 || this.x<0 || this.y<0){
        reset_counter++;
        if(reset_counter>100){
            context.clearRect(0,0,1920,1080);
            init();
            reset_counter=0;
        }
    }

    this.star++;

    this.x+=this.xsp;
    this.y+=this.ysp;

    if(this.star>6){
        this.star=0;
        context.clearRect(this.x,this.y,this.size*5.5,this.size*12.36);
    }

    
    context.drawImage(img,this.star*550,0,550,1236,this.x,this.y,this.size*5.5,this.size*12.36);
    
}

function init(){
    for(var i=0;i<count;i++){
        stars_array[i]=new Star();
        stars_array[i].init();
    }
}

function draw(){
    for(var j=0;j<count;j++){
        stars_array[j].draw();
    }
}

function my_onload(){
    
    //------------------初始化星星画布----------------
    can1=document.getElementById("can");
    context=can1.getContext("2d");

    init();
    setInterval(draw,80);
    setInterval(statistic,100);
  

    //----------------画出保存的建筑物-------------------
    var can3=document.getElementById("architecture");
    var context2=can3.getContext("2d");
    var lab=new Image();
    var rocket_pla=new Image();
    var high_tec=new Image();
    var ai=new Image();
    var array=[];

    lab.src="./图像/建筑/luxury-house-building-vecto.png";
    array=JSON.parse(localStorage.getItem("architec"));
    for(var i=0;i<array.length;)
    {
        var temp_x=parseInt(array[i]);
        var temp_y=parseInt(array[i+1]);

        context2.drawImage(lab,temp_x,temp_y,100,100);

        i=i+2;
    }

    rocket_pla.src="./图像/建筑/Rocket-Platform.png";
    array=JSON.parse(localStorage.getItem("architec2"));
    for(var i=0;i<array.length;)
    {
        var temp_x=parseInt(array[i]);
        var temp_y=parseInt(array[i+1]);

        context2.drawImage(rocket_pla,temp_x,temp_y,100,100);

        i=i+2;
    }

    high_tec.src="./图像/建筑/airport-building-exterior-v.png";
    array=JSON.parse(localStorage.getItem("architec3"));
    for(var i=0;i<array.length;)
    {
        var temp_x=parseInt(array[i]);
        var temp_y=parseInt(array[i+1]);

        context2.drawImage(high_tec,temp_x,temp_y,100,100);

        i=i+2;
    }

    ai.src="./图像/建筑/AI.png";
    array=JSON.parse(localStorage.getItem("architec4"));
    for(var i=0;i<array.length;)
    {
        var temp_x=parseInt(array[i]);
        var temp_y=parseInt(array[i+1]);

        context2.drawImage(ai,temp_x,temp_y,100,100);

        i=i+2;
    }
}