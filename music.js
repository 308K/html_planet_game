var count_state=0;

document.getElementById("music").onclick=function(){
    
    count_state++;

    var music=document.getElementById("bgmusic");
    var img=document.getElementById("music_img");

    if(count_state==1){

        music.play();

        img.style['animationPlayState']='running';

    }

    if(count_state==2){

        music.pause();

        img.style['animationPlayState']='paused';

        count_state=0;
    }
};

document.getElementById("music").onmousemove=function(){
    var state=document.getElementById("music_state");

    state.style.cursor="pointer";

    if(count_state==0){
        state.src="./图像/start.png";
        state.style.opacity="1";
    }

    if(count_state==1){
        state.src="./图像/stop.png";
        state.style.opacity="1";
    }
    
};

document.getElementById("music").onmouseleave=function(){
    var state=document.getElementById("music_state");

    state.style.opacity="0";
};