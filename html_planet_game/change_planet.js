var pur="./图像/planet/pur.png";
var air="./图像/planet/air.png";
var roc="./图像/planet/roc.png";
var dep="./图像/planet/dep.png";
var dia="./图像/planet/dia.png";
var phy="./图像/planet/phy.png";

var count=0;

document.getElementById("next").onmousemove=function(){
    var next=document.getElementById("next");

    next.style.cursor="pointer";
    next.style.opacity=1.0;
}

document.getElementById("next").onmouseleave=function(){
    var next=document.getElementById("next");

    next.style.opacity=0.5;
}

document.getElementById("next").onclick=function(){
    
    count++;

    if(count==6){count=0;}

    check_count();
}

document.getElementById("ex").onmousemove=function(){
    var ex=document.getElementById("ex");

    ex.style.cursor="pointer";
    ex.style.opacity=1.0;
}

document.getElementById("ex").onmouseleave=function(){
    var ex=document.getElementById("ex");

    ex.style.opacity=0.5;
}

document.getElementById("ex").onclick=function(){
    
    count--;

    if(count==-1){count=6;}

    check_count();
}


function check_count(){

    var planet=document.getElementById("core");

    switch(count){
        case 0:{
            planet.src=pur;
            break;
        }

        case 1:{
            planet.src=air;
            break;
        }

        case 2:{
            planet.src=roc;
            break;
        }

        case 3:{
            planet.src=dep;
            break;
        }

        case 4:{
            planet.src=dia;
            break;
        }

        case 5:{
            planet.src=cir;
            break;
        }
    }
}