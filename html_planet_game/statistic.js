document.getElementById("reset").onmousemove=function(){

    var reset_icon=document.getElementById("reset_icon");
    var reset=document.getElementById("reset");
    reset_icon.style['animationPlayState']='running';
    reset.style.cursor="pointer";

};

document.getElementById("reset").onmouseleave=function(){

    var reset_icon=document.getElementById("reset_icon");
    var reset=document.getElementById("reset");
    reset_icon.style['animationPlayState']='paused';
    reset.style.cursor="pointer";

};

document.getElementById("reset").onclick=function(){

    localStorage.clear();

};

function statistic(){

    var popultion=localStorage.getItem("population");
    var resource=localStorage.getItem("resource");
    var tecnology=localStorage.getItem("tecnology");

    var po=document.querySelector("h2");
    var re=document.querySelector("h3");
    var te=document.querySelector("h4");

    var population_incr=localStorage.getItem("pop_incr");
    var resource_incr=localStorage.getItem("res_incr");
    var tec_incr=localStorage.getItem("tec_incr");

    //localStorage.clear();
    if(popultion==null){
        popultion=7000000000;
        localStorage.setItem("population",7000000000);
    }
    if(resource==null){
        resource=200;
        localStorage.setItem("resource",200);
    }
    if(tecnology==null){
        tecnology=1;
        localStorage.setItem("tecnology",1);
    }

    if(population_incr==null){
        population_incr=1;
        localStorage.setItem("pop_incr",1);
    }
    if(resource_incr==null){
        resource_incr=200;
        localStorage.setItem("res_incr",200);
    }
    if(tec_incr==null){
        tec_incr=1;
        localStorage.setItem("tec_incr",1);
    }

    popultion=parseFloat(popultion);
    resource=parseFloat(resource);
    tecnology=parseFloat(tecnology);

    population_incr=parseFloat(population_incr);
    resource_incr=parseFloat(resource_incr);
    tec_incr=parseFloat(tec_incr);

    popultion=popultion+population_incr;
    resource=resource+resource_incr;
    tecnology=tecnology+tec_incr;

    localStorage.setItem("population",popultion);
    localStorage.setItem("resource",resource);
    localStorage.setItem("tecnology",tecnology);
    
    

    po.innerHTML="人口:   "+popultion+"   ("+population_incr+"/s)";
    re.innerHTML="资源:   "+resource+"   ("+resource_incr+"/s)";
    te.innerHTML="科技:   "+tecnology+"   ("+tec_incr+"/s)";
};