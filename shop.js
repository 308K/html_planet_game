var high_cost_res=50000;
var high_cost_tec=100;

var ai_cost_res=5000;
var ai_cost_tec=10000;

document.getElementById("lab").onmousemove=function(){
    var lab_buy=document.getElementById("lab_buy");
    var lab=document.getElementById("lab");
    var text=document.getElementById("lab_need");
    var res_cost=localStorage.getItem("lab_res_cost");
    var tec_cost=localStorage.getItem("lab_tec_cost");

    if(res_cost==null)
    {
        res_cost=30000;
        tec_cost=10;
        localStorage.setItem("lab_res_cost",res_cost);
        localStorage.setItem("lab_tec_cost",tec_cost);
    }

    lab.style.cursor="pointer";
    lab_buy.style['animation']='shop_show 1s linear';
    lab_buy.style['animationPlayState']='running';
    lab_buy.style['animation-fill-mode']='forwards';

    setTimeout(function(){
        text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;
    },300);
    
}

document.getElementById("lab").onmouseleave=function(){
    var lab_buy=document.getElementById("lab_buy");
    var text=document.getElementById("lab_need");

    setTimeout(function(){
        lab_buy.style['animation']='shop_close 1s linear';
    },800);
    
    setTimeout(function(){
        text.innerHTML="";
    },1200);
}

document.getElementById("lab").onclick=function(){

    var res=localStorage.getItem("resource");
    var tec=localStorage.getItem("tecnology");
    var res_inc=localStorage.getItem("res_incr");
    var tec_inc=localStorage.getItem("tec_incr");
    var res_cost=localStorage.getItem("lab_res_cost");
    var tec_cost=localStorage.getItem("lab_tec_cost");
    var text=document.getElementById("lab_need");
    var afroad=0;

    res=parseFloat(res);
    tec=parseFloat(tec);
    res_inc=parseFloat(res_inc);
    tec_inc=parseFloat(tec_inc);
    res_cost=parseFloat(res_cost);
    tec_cost=parseFloat(tec_cost);


    if(res>res_cost){
        if(tec>tec_cost)
        {
            afroad=1;
            localStorage.setItem("resource",res-res_cost);
            localStorage.setItem("tecnology",tec-tec_cost);
            localStorage.setItem("res_incr",res_inc+200);
            localStorage.setItem("tec_incr",tec_inc+5);

            res_cost=res_cost*5;
            tec_cost=tec_cost*5;

            localStorage.setItem("lab_res_cost",res_cost);
            localStorage.setItem("lab_tec_cost",tec_cost);

            text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;

        }else{
            afroad=0;
        }
    }else{
        afroad=0;
    }

    if(afroad==1){
        var canvas=document.getElementById("architecture");
        var context=canvas.getContext("2d");
        var img=new Image();

        var x=Math.random()*400;
        var y=Math.random()*400;

        x=parseInt(x);
        y=parseInt(y);

        var architec=JSON.parse(localStorage.getItem("architec"));
        if(architec==null)
        {
            var archi_array=[x,y];
            localStorage.setItem("architec",JSON.stringify(archi_array));
        }else{
            architec.push(x,y);
            localStorage.setItem("architec",JSON.stringify(architec));
        }

        img.src="./图像/建筑/luxury-house-building-vecto.png";
        context.drawImage(img,x,y,100,100);
    }

}








document.getElementById("rocket_pla").onmousemove=function(){
    var rocket_buy=document.getElementById("rocket_buy");
    var rocket_pla=document.getElementById("rocket_pla")
    var text=document.getElementById("rocket_need");
    var res_cost=localStorage.getItem("rocket_res_cost");
    var tec_cost=localStorage.getItem("rocket_tec_cost");

    if(res_cost==null)
    {
        res_cost=60000;
        tec_cost=300;
        localStorage.setItem("rocket_res_cost",res_cost);
        localStorage.setItem("rocket_tec_cost",tec_cost);
    }

    rocket_pla.style.cursor="pointer";
    rocket_buy.style['animation']='shop_show 1s linear';
    rocket_buy.style['animationPlayState']='running';
    rocket_buy.style['animation-fill-mode']='forwards';

    setTimeout(function(){
        text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;
    },300);

}

document.getElementById("rocket_pla").onmouseleave=function(){
    var rocket_buy=document.getElementById("rocket_buy");
    var text=document.getElementById("rocket_need");


    setTimeout(function(){
        rocket_buy.style['animation']='shop_close 1s linear';
    },800);

    setTimeout(function(){
        text.innerHTML="";
    },1200);
}

document.getElementById("rocket_pla").onclick=function(){

    var res=localStorage.getItem("resource");
    var tec=localStorage.getItem("tecnology");
    var res_inc=localStorage.getItem("res_incr");
    var tec_inc=localStorage.getItem("tec_incr");
    var res_cost=localStorage.getItem("rocket_res_cost");
    var tec_cost=localStorage.getItem("rocket_tec_cost");
    var text=document.getElementById("rocket_need");
    var afroad=0;

    res=parseFloat(res);
    tec=parseFloat(tec);
    res_inc=parseFloat(res_inc);
    tec_inc=parseFloat(tec_inc);
    res_cost=parseFloat(res_cost);
    tec_cost=parseFloat(tec_cost);


    if(res>res_cost){
        if(tec>tec_cost)
        {
            afroad=1;
            localStorage.setItem("resource",res-res_cost);
            localStorage.setItem("tecnology",tec-tec_cost);
            localStorage.setItem("res_incr",res_inc+200);
            localStorage.setItem("tec_incr",tec_inc+5);

            res_cost=res_cost*10;
            tec_cost=tec_cost*10;

            localStorage.setItem("rocket_res_cost",res_cost);
            localStorage.setItem("rocket_tec_cost",tec_cost);

            text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;

        }else{
            afroad=0;
        }
    }else{
        afroad=0;
    }

    if(afroad==1){
        var canvas=document.getElementById("architecture");
        var context=canvas.getContext("2d");
        var img=new Image();

        var x=Math.random()*400;
        var y=Math.random()*400;

        x=parseInt(x);
        y=parseInt(y);

        var architec=JSON.parse(localStorage.getItem("architec2"));
        if(architec==null)
        {
            var archi_array=[x,y];
            localStorage.setItem("architec2",JSON.stringify(archi_array));
        }else{
            architec.push(x,y);
            localStorage.setItem("architec2",JSON.stringify(architec));
        }

        img.src="./图像/建筑/Rocket-Platform.png";
        context.drawImage(img,x,y,100,100);
    }

}













document.getElementById("high_tec").onmousemove=function(){
    var high_buy=document.getElementById("high_buy");
    var high_tec=document.getElementById("high_tec");
    var text=document.getElementById("high_need");
    var res_cost=localStorage.getItem("high_res_cost");
    var tec_cost=localStorage.getItem("high_tec_cost");

    if(res_cost==null)
    {
        res_cost=50000;
        tec_cost=100;
        localStorage.setItem("high_res_cost",res_cost);
        localStorage.setItem("high_tec_cost",tec_cost);
    }

    high_tec.style.cursor="pointer";
    high_buy.style['animation']='shop_show 1s linear';
    high_buy.style['animationPlayState']='running';
    high_buy.style['animation-fill-mode']='forwards';

    setTimeout(function(){
        text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;
    },300);
}

document.getElementById("high_tec").onmouseleave=function(){
    var high_buy=document.getElementById("high_buy");
    var text=document.getElementById("high_need");

    setTimeout(function(){
        high_buy.style['animation']='shop_close 1s linear';
    },800);
    
    setTimeout(function(){
        text.innerHTML="";
    },1200);
}

document.getElementById("high_tec").onclick=function(){

    var res=localStorage.getItem("resource");
    var tec=localStorage.getItem("tecnology");
    var res_inc=localStorage.getItem("res_incr");
    var tec_inc=localStorage.getItem("tec_incr");
    var res_cost=localStorage.getItem("high_res_cost");
    var tec_cost=localStorage.getItem("high_tec_cost");
    var text=document.getElementById("high_need");
    var afroad=0;

    res=parseFloat(res);
    tec=parseFloat(tec);
    res_inc=parseFloat(res_inc);
    tec_inc=parseFloat(tec_inc);
    res_cost=parseFloat(res_cost);
    tec_cost=parseFloat(tec_cost);


    if(res>res_cost){
        if(tec>tec_cost)
        {
            afroad=1;
            localStorage.setItem("resource",res-res_cost);
            localStorage.setItem("tecnology",tec-tec_cost);
            localStorage.setItem("res_incr",res_inc+200);
            localStorage.setItem("tec_incr",tec_inc+5);

            res_cost=res_cost*5;
            tec_cost=tec_cost*5;

            localStorage.setItem("high_res_cost",res_cost);
            localStorage.setItem("high_tec_cost",tec_cost);

            text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;

        }else{
            afroad=0;
        }
    }else{
        afroad=0;
    }

    if(afroad==1){
        var canvas=document.getElementById("architecture");
        var context=canvas.getContext("2d");
        var img=new Image();

        var x=Math.random()*400;
        var y=Math.random()*400;

        x=parseInt(x);
        y=parseInt(y);

        var architec=JSON.parse(localStorage.getItem("architec3"));
        if(architec==null)
        {
            var archi_array=[x,y];
            localStorage.setItem("architec3",JSON.stringify(archi_array));
        }else{
            architec.push(x,y);
            localStorage.setItem("architec3",JSON.stringify(architec));
        }

        img.src="./图像/建筑/airport-building-exterior-v.png";
        context.drawImage(img,x,y,100,100);
    }

}














document.getElementById("ai").onmousemove=function(){
    var ai_buy=document.getElementById("ai_buy");
    var ai=document.getElementById("ai");
    var text=document.getElementById("ai_need");
    var res_cost=localStorage.getItem("ai_res_cost");
    var tec_cost=localStorage.getItem("ai_tec_cost");

    if(res_cost==null)
    {
        res_cost=5000;
        tec_cost=10000;
        localStorage.setItem("ai_res_cost",res_cost);
        localStorage.setItem("ai_tec_cost",tec_cost);
    }

    ai.style.cursor="pointer";
    ai_buy.style['animation']='shop_show 1s linear';
    ai_buy.style['animationPlayState']='running';
    ai_buy.style['animation-fill-mode']='forwards';

    setTimeout(function(){
        text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;
    },300);
}

document.getElementById("ai").onmouseleave=function(){
    var ai_buy=document.getElementById("ai_buy");
    var text=document.getElementById("ai_need");

    setTimeout(function(){
        ai_buy.style['animation']='shop_close 1s linear';
    },800);

    setTimeout(function(){
        text.innerHTML="";
    },1200);
    
}

document.getElementById("ai").onclick=function(){

    var res=localStorage.getItem("resource");
    var tec=localStorage.getItem("tecnology");
    var res_inc=localStorage.getItem("res_incr");
    var tec_inc=localStorage.getItem("tec_incr");
    var po_inc=localStorage.getItem("pop_incr");
    var res_cost=localStorage.getItem("ai_res_cost");
    var tec_cost=localStorage.getItem("ai_tec_cost");
    var text=document.getElementById("ai_need");
    var afroad=0;

    res=parseFloat(res);
    tec=parseFloat(tec);
    res_inc=parseFloat(res_inc);
    tec_inc=parseFloat(tec_inc);
    po_inc=parseFloat(po_inc);
    res_cost=parseFloat(res_cost);
    tec_cost=parseFloat(tec_cost);


    if(res>res_cost){
        if(tec>tec_cost)
        {
            afroad=1;
            localStorage.setItem("resource",res-res_cost);
            localStorage.setItem("tecnology",tec-tec_cost);
            localStorage.setItem("res_incr",res_inc+200);
            localStorage.setItem("tec_incr",tec_inc+5);
            localStorage.setItem("pop_incr",po_inc-10000);

            res_cost=res_cost*10;
            tec_cost=tec_cost*10;

            localStorage.setItem("ai_res_cost",res_cost);
            localStorage.setItem("ai_tec_cost",tec_cost);

            text.innerHTML="消耗 资源:"+res_cost+"  科技:"+tec_cost;

        }else{
            afroad=0;
        }
    }else{
        afroad=0;
    }

    if(afroad==1){
        var canvas=document.getElementById("architecture");
        var context=canvas.getContext("2d");
        var img=new Image();

        var x=Math.random()*400;
        var y=Math.random()*400;

        x=parseInt(x);
        y=parseInt(y);

        var architec=JSON.parse(localStorage.getItem("architec4"));
        if(architec==null)
        {
            var archi_array=[x,y];
            localStorage.setItem("architec4",JSON.stringify(archi_array));
        }else{
            architec.push(x,y);
            localStorage.setItem("architec4",JSON.stringify(architec));
        }

        img.src="./图像/建筑/AI.png";
        context.drawImage(img,x,y,100,100);
    }

}













document.getElementById("shop").onmousemove=function(){
    var shop=document.getElementById("shop");
    shop.style.opacity=1.0;
}

document.getElementById("shop").onmouseleave=function(){
    var shop=document.getElementById("shop");
    shop.style.opacity=0.5;
}