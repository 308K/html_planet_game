document.getElementById("ball_info").onmousemove=function(){
    
    var ball=document.getElementById("ball");
    var info=document.getElementById("info");
    var img=document.getElementById("info_img");

    ball.style['animationPlayState']='paused';
    ball.style['-webkit-filter']='drop-shadow(10px 10px 10px rgba(0,0,0,.5))';

    info.style['animation']='show 1s linear';
    info.style['animationPlayState']='running';
    info.style['animation-fill-mode']='forwards';
    info.style['-webkit-filter']='drop-shadow(10px 10px 10px rgba(0,0,0,.5))';

    img.style.width="500px";
    img.style['animation']='fade_in 3s';

};

document.getElementById("ball_info").onmouseleave=function(){

    var ball=document.getElementById("ball");
    var info=document.getElementById("info");
    var img=document.getElementById("info_img");
    
    
    ball.style['animationPlayState']='running';
    ball.style['-webkit-filter']='drop-shadow(0px 0px 0px rgba(0,0,0,0))';

    
    
    info.style['animationPlayState']='running';
    info.style['-webkit-filter']='drop-shadow(0px 0px 0px rgba(0,0,0,0))';

    setTimeout(function(){
        info.style['animation']='close 1s linear';
        img.style['animation']='fade_out 1s';
    },800);
    
    
    setTimeout(function(){
        img.style.width="0px";
      },1600);

    context.clearRect(0,0,1000,1000);

};